/* 
 * File:   main.c
 * Author: karen
 *
 * Created on 3 de febrero de 2016, 09:34 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

/*
 * 
 */
struct Particion
{
    char part_status;
    char part_type;
    char part_fit;
    int part_start;
    int part_size;
    char part_name[16];
};

struct MBR
{
    int mbr_tamanio;
    char fecha[50];
    int mbr_disk_signature;
    struct Particion mbr_particion1;
    struct Particion mbr_particion2;
    struct Particion mbr_particion3;
    struct Particion mbr_particion4;
};

struct EBR
{
    char part_status;
    char part_fit;
    int part_start;
    int part_size;
    int part_next;
    char part_name[16];
};

void str_lw( char* s );
long filesize(FILE *f);
char* FechaHora();

int main(int argc, char** argv) {
   
    int ex = 0;
    int i = 0; 
    char arreglo[50][50];
    
    while(ex == 0)
    {
        char cadena[50];
        char separador[] = " -=\"\t";
        char *resto = NULL;
        printf("Ingrese un comando: "); 
        //scanf("%s",cadena);  
        gets(cadena);
        
        //printf("La cadena ingresada es: %s\n",cadena);
        //gets(resto);
        //printf("el resto es: %s\n",resto);
        //puts(resto);

        resto = strtok (cadena, separador);

        while (resto != NULL)
        {
            str_lw(resto);
            //printf("%s\n",resto);
            strcpy(arreglo[i], resto);
            //printf ("trozo: %s\n",resto);     // Aqui deberias guardar tu dato en el array!
            //printf ("%s\n", arreglo[i]);
            resto = strtok (NULL, separador);  // Aca tambien iria solo la coma.!!
            i++; 
        }
        
        int x = 0;
        //int a =strcmp("mkdisk",arreglo[x]);
        
        if(strcmp("\\",arreglo[i-1]) == 0)
        {
            //continua leendo
            strcpy(arreglo[i-1],"");
            i = i-1;
        }
        else
        {
            if(strcmp("mkdisk",arreglo[x])== 0)
            {
                int posSize = 0;
                int posUnit = 0;
                int posPath = 0;
                int size = 0;
                int tipoSize = 1024*1024;
                char path[100];

                for(x=1; x<i; x++)
                {
                    if(strcmp("size",arreglo[x]) == 0)
                    {             
                        posSize = x;
                        if(atoi(arreglo[x+1]) > 0)
                            size = atoi(arreglo[x+1]);
                        else
                            printf("no es digito %s\n",arreglo[x+1]);

                    }
                    else if(strcmp("unit",arreglo[x]) == 0)
                    {
                        posUnit = x;
                        if(strcmp("k",arreglo[x+1])== 0)
                        {
                            tipoSize = 1024;
                        }
                        else if(strcmp("m",arreglo[x+1]) == 0)
                        {
                            tipoSize = 1024 * 10224;
                        }
                        else
                        {
                            printf("Error el valor %s no es valido", arreglo[x+1]);
                        }

                    }
                    else if(strcmp("path",arreglo[x]) == 0)
                    {
                        posPath = x;
                        strcpy(path, arreglo[x+1]);
                    }
                    else
                    {
                        if(strcmp("path",arreglo[x-2])== 0)
                        {
                            strcpy(path, arreglo[x-1]);
                            strcat(path, " ");
                            strcat(path, arreglo[x]);
                        }
                        else
                        {
                            printf("Este comando no esta definido en mkdisk %s", arreglo[x]);
                        }
                        x = x-1;
                    }
                    x = x+1;
                }

                char directorio[100] ="/";
                char arreglo2[50][50];
                char separador2[] = "\"/";
                char *resto2 = NULL;
                char path1[100];
                int t = 0;
                
                strcpy(path1, path);
                resto2 = strtok (path, separador2);
                
                while (resto2 != NULL)
                {
                    strcpy(arreglo2[t], resto2);
                    resto2 = strtok (NULL, separador2);  // Aca tambien iria solo la coma.!!
                    t++; 
                }
                
                if(strcmp(arreglo2[2], "user") == 0)
                {
                    strcpy(arreglo2[2], "karen");
                }
                
                int j;
                for(j=0; j<t-1; j++)
                {
                    strcat(directorio, arreglo2[j]);
                    strcat(directorio, "/");
                    mkdir(directorio, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
                }
                
                struct Particion prt1, prt2, prt3, prt4;
                
                strcpy(prt1.part_name, "x");
                prt1.part_fit = '0';
                prt1.part_size = 0;
                prt1.part_start = 0;
                prt1.part_status = '0';
                prt1.part_type = '0';
                
                strcpy(prt2.part_name, "x");
                prt2.part_fit = '0';
                prt2.part_size = 0;
                prt2.part_start = 0;
                prt2.part_status = '0';
                prt2.part_type = '0';
                
                strcpy(prt3.part_name, "x");
                prt3.part_fit = '0';
                prt3.part_size = 0;
                prt3.part_start = 0;
                prt3.part_status = '0';
                prt3.part_type = '0';
                
                strcpy(prt4.part_name, "x");
                prt4.part_fit = '0';
                prt4.part_size = 0;
                prt4.part_start = 0;
                prt4.part_status = '0';
                prt4.part_type = '0';
                
                int tam = size * tipoSize -sizeof(struct MBR);
                
                struct MBR nuevo;
                nuevo.mbr_tamanio = tam;
                strcpy(nuevo.fecha, FechaHora());
                nuevo.mbr_disk_signature = 1 + rand()%((100-1)+1);
                nuevo.mbr_particion1 = prt1;
                nuevo.mbr_particion2 = prt2;
                nuevo.mbr_particion3 = prt3;
                nuevo.mbr_particion4 = prt4;
                                
                 FILE * f = fopen(path1, "w+b");
                 if(f)
                 {
                    char cad[] = "\0";
                    fseek(f,0,SEEK_SET);
                    fwrite(&nuevo, sizeof(struct MBR), 1, f);                    
                    int z ;
                    for(z = 0; z<tam; z++)
                    {
                      fwrite(cad, strlen(cad)+1, 1, f);
                    }
                    printf("El tamaño de mi fichero en bytes es %ld\n", filesize(f));
                    //fclose(f);
                 }
                 else
                 {
                     printf("No se pudo crear el archivo \n");
                 }
                 fclose(f);
            }
            else if(strcmp("rmdisk",arreglo[x]) == 0)
            {
                if(strcmp("path",arreglo[1])== 0)
                {
                    char path[100];
                    if(i == 4)
                    {
                        strcpy(path, arreglo[2]);
                        strcat(path, " ");
                        strcat(path, arreglo[3]);
                    }
                    else
                    {
                       strcpy(path, arreglo[2]);
                    }
                    
                     if(remove(path)== 0)
                     {
                        printf("El archivo fue eliminado satisfactoriamente\n");   
                     }
                     else
                     {
                        printf("No se pudo eliminar el archivo\n"); 
                     }
                }
                else
                {
                    printf("Este comando no esta definido en rmdisk %s \n", arreglo[1]);
                }
            }
            else if(strcmp("fdisk",arreglo[x])== 0)
            {
                int posSize = 0;
                int posUnit = 0;
                int posPath = 0;
                int posType = 0;
                int posFit = 0;
                int posDelete = 0;
                int posName = 0;
                int posAdd = 0;
                int size = 0;
                int tipoSize = 1024;
                char path[100];
                int tipoParticion = 0;
                int tipoAjuste = 3;
                int tipoDele = 0;

                for(x=1; x<i; x++)
                {
                    if(strcmp("size",arreglo[x]) == 0)
                    {
                        posSize = x;
                        //verificar si son numeros
                        if(atoi(arreglo[x+1]) > 0)
                            size = atoi(arreglo[x+1]);
                        else
                            printf("no es digito %s\n",arreglo[x+1]);

                    }
                    else if(strcmp("unit",arreglo[x]) == 0)
                    {
                        posUnit = x;
                        if(strcmp("b",arreglo[x+1]) == 0)
                        {
                            tipoSize = 1;
                        }
                        else if(strcmp("k",arreglo[x+1]) == 0)
                        {
                            tipoSize = 1024;
                        }
                        else if(strcmp("m",arreglo[x+1]) == 0)
                        {
                            tipoSize = 1024 * 1024;
                        }
                        else
                        {
                            printf("Error el valor %s no es valido",arreglo[x+1]);
                        }
                    }
                    else if(strcmp("path",arreglo[x]) == 0)
                    {
                        posPath = x;
                        strcpy(path, arreglo[x+1]);
                    }
                    else if(strcmp("type",arreglo[x]) == 0)
                    {
                        posType = x;
                        if(strcmp("p",arreglo[x+1]) == 0)
                        {
                            tipoParticion = 0;
                        }
                        else if(strcmp("e",arreglo[x+1]) == 0)
                        {
                            tipoParticion = 1;
                        }
                        else if(strcmp("l",arreglo[x+1])== 0)
                        {
                            tipoParticion = 2;
                        }
                        else
                        {
                            printf("Error el valor %s no es valido",arreglo[x+1]);
                        }
                    }
                    else if(strcmp("fit",arreglo[x]) == 0)
                    {
                        posFit = x;
                        if(strcmp("bf",arreglo[x+1]) == 0)
                        {
                            tipoAjuste = 1;
                        }
                        else if(strcmp("ff",arreglo[x+1]) == 0)
                        {
                            tipoAjuste = 2;
                        }
                        else if(strcmp("wf",arreglo[x+1]) == 0)
                        {
                            tipoAjuste = 3;
                        }
                        else
                        {
                            printf("Error el valor %s no es valido",arreglo[x+1]);
                        }
                    }
                    else if(strcmp("delete",arreglo[x]) == 0)
                    {
                        posDelete = x;
                        if(strcmp("fast",arreglo[x+1]) == 0)
                        {
                            tipoDele =0;
                        }
                        else if(strcmp("full",arreglo[x+1]) == 0)
                        {
                            tipoDele =1;
                        }
                        else
                        {
                            printf("Error el valor %s no es valido",arreglo[x+1]);
                        }
                    }
                    else if(strcmp("name",arreglo[x]) == 0)
                    {
                        posName = x;
                    }
                    else if(strcmp("add",arreglo[x]) == 0)
                    {
                        posAdd = x;
                        
                    }
                    else
                    {
                         if(strcmp("path",arreglo[x-2])== 0)
                        {
                            strcpy(path, arreglo[x+1]);
                            strcat(path, " ");
                            strcat(path, arreglo[x+2]);
                        }
                        else
                        {
                           printf("Este comando no de encuentra en fdisk %s \n", arreglo[x]);
                        }
                        x = x-1;
                    }
                    x = x+1;
                }
                
                //leer archivo para crear particion
                struct MBR archivo;
                
                FILE *f = fopen(path, "w+b");
                if(f)
                {
                    fseek(f,0,SEEK_SET);
                    fread(&archivo, sizeof(struct MBR), 1, f);
                    
                    /*char b[50];
                     strcpy(b,archivo.fecha);
                    int c = archivo.mbr_disk_signature;
                    int d = archivo.mbr_tamanio;
                    char a[16];
                    strcpy(a, archivo.mbr_particion1.part_name);
                    char e = archivo.mbr_particion1.part_fit;
                    char f[16]; 
                    strcpy(f, archivo.mbr_particion1.part_name);
                    int g = archivo.mbr_particion1.part_size;
                    int h = archivo.mbr_particion1.part_start;
                    char il = archivo.mbr_particion1.part_status;
                    char jk = archivo.mbr_particion1.part_type;
                    */
                    
                    int hayExtendida = 0;
                    if(posDelete != 0)
                    {
                        int si =0;
                        if(strcmp(archivo.mbr_particion1.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion1.part_name, arreglo[posName+1]))
                            {
                                si =1;

                                if(tipoDele == 0)
                                    archivo.mbr_particion1.part_status = '0';
                                else
                                {
                                    struct Particion part;
                                    part.part_fit = '0';
                                    strcpy(part.part_name, "x");
                                    part.part_size = 0;
                                    part.part_start = 0;
                                    part.part_type = '0';
                                    
                                    archivo.mbr_particion1 = archivo.mbr_particion2;
                                    archivo.mbr_particion2 = archivo.mbr_particion3;
                                    archivo.mbr_particion3 = archivo.mbr_particion4;
                                    archivo.mbr_particion4 = part;
                                }

                                fseek(f,0,SEEK_SET);
                                fwrite(&archivo,sizeof(struct MBR), 1, f);
                            }
                        }
                        if(strcmp(archivo.mbr_particion2.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion2.part_name, arreglo[posName+1]))
                            {
                                si =1;
                                if(tipoDele == 0)
                                    archivo.mbr_particion2.part_status = '0';
                                else
                                {
                                    struct Particion part;
                                    part.part_fit = '0';
                                    strcpy(part.part_name, "x");
                                    part.part_size = 0;
                                    part.part_start = 0;
                                    part.part_type = '0';
                                    
                                    archivo.mbr_particion2 = archivo.mbr_particion3;
                                    archivo.mbr_particion3 = archivo.mbr_particion4;
                                    archivo.mbr_particion4 = part;
                                }

                                fseek(f,0,SEEK_SET);
                                fwrite(&archivo,sizeof(struct MBR), 1, f);
                            }
                        }
                        if(strcmp(archivo.mbr_particion3.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion3.part_name, arreglo[posName+1]))
                            {
                                si =1;
                                if(tipoDele == 0)
                                    archivo.mbr_particion3.part_status = '0';
                                else
                                {
                                    struct Particion part;
                                    part.part_fit = '0';
                                    strcpy(part.part_name, "x");
                                    part.part_size = 0;
                                    part.part_start = 0;
                                    part.part_type = '0';
                                    
                                    archivo.mbr_particion3 = archivo.mbr_particion4;
                                    archivo.mbr_particion4 = part;
                                }
                                fseek(f,0,SEEK_SET);
                                fwrite(&archivo,sizeof(struct MBR), 1, f);
                            }
                        }
                        if(strcmp(archivo.mbr_particion4.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion4.part_name, arreglo[posName+1]))
                            {
                                si =1;
                                if(tipoDele == 0)
                                    archivo.mbr_particion4.part_status = '0';
                                else
                                {
                                    struct Particion part;
                                    part.part_fit = '0';
                                    strcpy(part.part_name, "x");
                                    part.part_size = 0;
                                    part.part_start = 0;
                                    part.part_type = '0';
                                    
                                    archivo.mbr_particion4 = part;
                                }

                                fseek(f,0,SEEK_SET);
                                fwrite(&archivo,sizeof(struct MBR), 1, f);
                            }
                        }
                        else
                        {
                            printf("Error la particion %s no existe en el disco \n", arreglo[posName+1]);
                        }
                    }
                    else if(posAdd != 0)
                    {
                        int add;
                        int si =0;
                        if(strcmp(archivo.mbr_particion1.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion1.part_name, arreglo[posName+1]))
                            {
                                si =1;
                                if(atoi(arreglo[posAdd+1])!= 0)
                                {
                                    add = atoi(arreglo[posAdd+1])*tipoSize;
                                
                                    if(atoi(arreglo[posAdd+1]) < 0 && (archivo.mbr_particion1.part_size + add) >= 0)
                                    {
                                        archivo.mbr_particion1.part_size = archivo.mbr_particion1.part_size + add;
                                        fseek(f,0,SEEK_SET);
                                        fwrite(&archivo,sizeof(struct MBR), 1, f);
                                    }
                                    else if(atoi(arreglo[posAdd+1]) > 0)
                                    {
                                        //se compara si la particion siguiente tiene espacio
                                    }
                                    else
                                    {
                                        printf("Error no se puede add = %s la particion \n",arreglo[posAdd+1]);
                                    }
                                }
                                else
                                    printf("no es digito %s\n",arreglo[x+1]);
                            }
                        }
                        if(strcmp(archivo.mbr_particion2.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion2.part_name, arreglo[posName+1]))
                            {
                                si =1;
                                
                                if(atoi(arreglo[posAdd+1])!= 0)
                                {
                                    add = atoi(arreglo[posAdd+1])*tipoSize;
                                
                                    if(atoi(arreglo[posAdd+1]) < 0 && (archivo.mbr_particion2.part_size + add) >= 0)
                                    {
                                        archivo.mbr_particion2.part_size = archivo.mbr_particion2.part_size + add;
                                        fseek(f,0,SEEK_SET);
                                        fwrite(&archivo,sizeof(struct MBR), 1, f);
                                    }
                                    else if(atoi(arreglo[posAdd+1]) > 0)
                                    {
                                        //se compara si la particion siguiente tiene espacio
                                    }
                                    else
                                    {
                                        printf("Error no se puede add = %s la particion \n",arreglo[posAdd+1]);
                                    }
                                }
                                else
                                    printf("no es digito %s\n",arreglo[x+1]);
                            }
                        }
                        if(strcmp(archivo.mbr_particion3.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion3.part_name, arreglo[posName+1]))
                            {
                                si =1;
                                if(atoi(arreglo[posAdd+1])!= 0)
                                {
                                    add = atoi(arreglo[posAdd+1])*tipoSize;
                                
                                    if(atoi(arreglo[posAdd+1]) < 0 && (archivo.mbr_particion3.part_size + add) >= 0)
                                    {
                                        archivo.mbr_particion3.part_size = archivo.mbr_particion3.part_size + add;
                                        fseek(f,0,SEEK_SET);
                                        fwrite(&archivo,sizeof(struct MBR), 1, f);
                                    }
                                    else if(atoi(arreglo[posAdd+1]) > 0)
                                    {
                                        //se compara si la particion siguiente tiene espacio
                                    }
                                    else
                                    {
                                        printf("Error no se puede add = %s la particion \n",arreglo[posAdd+1]);
                                    }
                                }
                                else
                                    printf("no es digito %s\n",arreglo[x+1]);
                            }
                        }
                        if(strcmp(archivo.mbr_particion4.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion4.part_name, arreglo[posName+1]))
                            {
                                si =1;
                                if(atoi(arreglo[posAdd+1])!= 0)
                                {
                                    add = atoi(arreglo[posAdd+1])*tipoSize;
                                
                                    if(atoi(arreglo[posAdd+1]) < 0 && (archivo.mbr_particion4.part_size + add) >= 0)
                                    {
                                        archivo.mbr_particion4.part_size = archivo.mbr_particion4.part_size + add;
                                        fseek(f,0,SEEK_SET);
                                        fwrite(&archivo,sizeof(struct MBR), 1, f);
                                    }
                                    else if(atoi(arreglo[posAdd+1]) > 0)
                                    {
                                        //se compara si la particion siguiente tiene espacio
                                    }
                                    else
                                    {
                                        printf("Error no se puede add = %s la particion \n",arreglo[posAdd+1]);
                                    }
                                }
                                else
                                    printf("no es digito %s\n",arreglo[x+1]);
                            }
                        }
                        else
                        {
                            printf("Error la particion %s no existe en el disco \n", arreglo[posName+1]);
                        }
                    }
                    else
                    {
                        int si = 0;
                        if(strcmp(archivo.mbr_particion1.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion1.part_type, 'E') == 0)
                            {
                                hayExtendida = 1;
                            }
                            if(strcmp(archivo.mbr_particion1.part_type, 'E') == 0 && tipoParticion == 1)
                            {
                                si = 1;
                                printf("Error ya existe una particion Extendida en el disco");
                            }
                            if(strcmp(archivo.mbr_particion1.part_name, arreglo[posName+1]) == 0)
                            {
                                si = 1;
                                printf("Error ya existe una particion con el mismo nombre %s\n",arreglo[posName+1]);
                            }
                            if(strcmp(archivo.mbr_particion1.part_type, 'E') == 0 && tipoParticion == 2)
                            {
                                archivo.mbr_particion1.part_start;
                            }
                        }
                        if(strcmp(archivo.mbr_particion2.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion2.part_type, 'E') == 0)
                            {
                                hayExtendida = 1;
                            }
                            if(strcmp(archivo.mbr_particion2.part_type, 'E') == 0 && tipoParticion == 1)
                            {
                                si = 1;
                                printf("Error ya existe una particion Extendida en el disco");
                            }
                            if(strcmp(archivo.mbr_particion2.part_name, arreglo[posName+1]) == 0)
                            {
                                si = 1;
                                printf("Error ya existe una particion con el mismo nombre %s\n",arreglo[posName+1]);
                            }
                            if(strcmp(archivo.mbr_particion2.part_type, 'E') == 0 && tipoParticion == 2)
                            {

                            }
                        }
                        if(strcmp(archivo.mbr_particion3.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion3.part_type, 'E') == 0)
                            {
                                hayExtendida = 1;
                            }
                            if(strcmp(archivo.mbr_particion3.part_type, 'E') == 0 && tipoParticion == 1)
                            {
                                si = 1;
                                printf("Error ya existe una particion Extendida en el disco");
                            }
                            if(strcmp(archivo.mbr_particion3.part_name, arreglo[posName+1]) == 0)
                            {
                                si = 1;
                                printf("Error ya existe una particion con el mismo nombre %s\n",arreglo[posName+1]);
                            }
                            if(strcmp(archivo.mbr_particion3.part_type, 'E') == 0 && tipoParticion == 2)
                            {

                            }
                        }
                        if(strcmp(archivo.mbr_particion4.part_status, '1') == 0 && si == 0)
                        {
                            if(strcmp(archivo.mbr_particion4.part_type, 'E') == 0)
                            {
                                hayExtendida = 1;
                            }
                            if(strcmp(archivo.mbr_particion4.part_type, 'E') == 0 && tipoParticion == 1)
                            {
                                si = 1;
                                printf("Error ya existe una particion Extendida en el disco");
                            }
                            if(strcmp(archivo.mbr_particion4.part_name, arreglo[posName+1]) == 0)
                            {
                                si = 1;
                                printf("Error ya existe una particion con el mismo nombre %s\n",arreglo[posName+1]);
                            }
                            if(strcmp(archivo.mbr_particion4.part_type, 'E') == 0 && tipoParticion == 2)
                            {

                            }
                        }
                        else
                        {
                            // si es nula o y el tipo
                            if(hayExtendida != 1 && tipoParticion == 2)
                            {
                                printf("Error no se puede crear una particion logica porque no existe la Extendida en el disco \n");
                            }
                            else if(strcmp(archivo.mbr_particion1.part_status, '0') == 0)
                            {
                                struct Particion part;

                                if(tipoAjuste == 3)
                                    part.part_fit = 'W';
                                else if(tipoAjuste == 2)
                                    part.part_fit = 'F';
                                else 
                                    part.part_fit = 'B';

                                strcpy(part.part_name, arreglo[posName+1]);
                                part.part_size = size;
                                part.part_start = sizeof(struct MBR);
                                part.part_status = '1';

                                if(tipoParticion == 1)
                                {
                                    part.part_type = 'E';
                                    
                                    struct EBR ebr;
                                    ebr.part_fit = '0';
                                    strcpy(ebr.part_name, "x"); 
                                    ebr.part_next = -1;
                                    ebr.part_size = 0;
                                    ebr.part_start = 0;
                                    ebr.part_status = '0';
                                    
                                    fseek(f,part.part_start, SEEK_SET);
                                    fwrite(&ebr, sizeof(struct EBR), 1, f);
                                }
                                else if(tipoParticion == 0)
                                    part.part_type = 'P';

                                archivo.mbr_particion1 = part;
                                fseek(f,0,SEEK_SET);
                                fwrite(&archivo, sizeof(struct MBR), 1, f);
                            }
                            else if(strcmp(archivo.mbr_particion2.part_status, '0') == 0)
                            {
                                struct Particion part;

                                if(tipoAjuste == 3)
                                    part.part_fit = 'W';
                                else if(tipoAjuste == 2)
                                    part.part_fit = 'F';
                                else 
                                    part.part_fit = 'B';

                                strcpy(part.part_name, arreglo[posName+1]);
                                part.part_size = size;
                                part.part_start = archivo.mbr_particion1.part_start + archivo.mbr_particion1.part_size;
                                part.part_status = '1';

                                 if(tipoParticion == 1)
                                {
                                    part.part_type = 'E';
                                    struct EBR ebr;
                                    ebr.part_fit = '0';
                                    strcpy(ebr.part_name, "x"); 
                                    ebr.part_next = -1;
                                    ebr.part_size = 0;
                                    ebr.part_start = 0;
                                    ebr.part_status = '0';
                                    
                                    fseek(f,part.part_start, SEEK_SET);
                                    fwrite(&ebr, sizeof(struct EBR), 1, f);
                                }
                                else if(tipoParticion == 0)
                                    part.part_type = 'P';

                                archivo.mbr_particion2 = part;
                                fseek(f,0,SEEK_SET);
                                fwrite(&archivo, sizeof(struct MBR), 1, f);
                            }
                            else if(strcmp(archivo.mbr_particion3.part_status, '0') == 0)
                            {
                                struct Particion part;

                                if(tipoAjuste == 3)
                                    part.part_fit = 'W';
                                else if(tipoAjuste == 2)
                                    part.part_fit = 'F';
                                else 
                                    part.part_fit = 'B';

                                strcpy(part.part_name, arreglo[posName+1]);
                                part.part_size = size;
                                part.part_start = archivo.mbr_particion2.part_start + archivo.mbr_particion2.part_size;
                                part.part_status = '1';

                                if(tipoParticion == 1)
                                {
                                    part.part_type = 'E';
                                    struct EBR ebr;
                                    ebr.part_fit = '0';
                                    strcpy(ebr.part_name, "x"); 
                                    ebr.part_next = -1;
                                    ebr.part_size = 0;
                                    ebr.part_start = 0;
                                    ebr.part_status = '0';
                                    
                                    fseek(f,part.part_start, SEEK_SET);
                                    fwrite(&ebr, sizeof(struct EBR), 1, f);
                                }
                                else if(tipoParticion == 0)
                                    part.part_type = 'P';

                                archivo.mbr_particion3 = part;
                                fseek(f,0,SEEK_SET);
                                fwrite(&archivo, sizeof(struct MBR), 1, f);
                            }
                            else if(strcmp(archivo.mbr_particion4.part_status, '0') == 0)
                            {
                                struct Particion part;

                                if(tipoAjuste == 3)
                                    part.part_fit = 'W';
                                else if(tipoAjuste == 2)
                                    part.part_fit = 'F';
                                else 
                                    part.part_fit = 'B';

                                strcpy(part.part_name, arreglo[posName+1]);
                                part.part_size = size;
                                part.part_start = archivo.mbr_particion3.part_start + archivo.mbr_particion3.part_size;
                                part.part_status = '1';

                                if(tipoParticion == 1)
                                {
                                    part.part_type = 'E';
                                    struct EBR ebr;
                                    ebr.part_fit = '0';
                                    strcpy(ebr.part_name, "x"); 
                                    ebr.part_next = -1;
                                    ebr.part_size = 0;
                                    ebr.part_start = 0;
                                    ebr.part_status = '0';
                                    
                                    fseek(f,part.part_start, SEEK_SET);
                                    fwrite(&ebr, sizeof(struct EBR), 1, f);
                                }
                                else if(tipoParticion == 0)
                                    part.part_type = 'P';

                                archivo.mbr_particion4 = part;
                                fseek(f,0,SEEK_SET);
                                fwrite(&archivo, sizeof(struct MBR), 1, f);
                            } 
                            else
                            {
                                printf("Error ya hay 4 particiones en el disco no se pueden crear mas \n");
                            }
                        }
                    }
                }
                else
                {
                    printf("No se pudo hacer la particion \n");
                }
                fclose(f);
            }
            else if(strcmp("mount",arreglo[x]) == 0)
            {
                int posPath = 0;
                int posName = 0;

                for(x=1; x<i; x++)
                {
                    if(strcmp("path",arreglo[x]) == 0)
                    {
                        posPath = x;
                    }
                    else if(strcmp("name",arreglo[x]) == 0)
                    {
                        posName = x;
                    }
                    else
                    {
                        printf("Este comando no existe en mount %s \n",arreglo[x]);
                    }
                    x = x+1;
                }

            }
            else if(strcmp("unmount",arreglo[x]) == 0)
            {
                if(strcmp("id",arreglo[1]) == 0)
                {

                }
                else
                {
                    printf("Este comando no existe en unmount %s \n",arreglo[1]);
                }
            }
            else if(strcmp("rep",arreglo[x]) == 0)
            {
                int posId = 0;
                int posPath = 0;
                int posName = 0;
                char path[100];

                for(x=1; x<i; x++)
                {
                    if(strcmp("id",arreglo[x]) == 0)
                    {
                        posId = x;
                    }
                    else if(strcmp("path",arreglo[x]) == 0)
                    {
                        posPath = x;
                    }
                    else if(strcmp("name",arreglo[x]) == 0)
                    {
                        posName = x;
                        if(strcmp("mbr",arreglo[x+1]) == 0)
                        {

                        }
                        else if(strcmp("disk",arreglo[x+1]) == 0)
                        {

                        }
                        else
                        {
                            printf("Error el valor %s no es valido",arreglo[x+1]);
                        }
                    }
                    else
                    {
                        printf("Este comando no existe en rep %s \n",arreglo[x]);
                    }
                    x = x+1;
                    
                    char directorio[100] ="/";
                    char arreglo2[50][50];
                    char separador2[] = "/";
                    char *resto2 = NULL;
                    char path1[100];
                    int t = 0;
                
                    strcpy(path1, path);
                    resto2 = strtok (path, separador2);
                
                    while (resto2 != NULL)
                    {
                        strcpy(arreglo2[t], resto2);
                        resto2 = strtok (NULL, separador2);  // Aca tambien iria solo la coma.!!
                        t++; 
                    }
                
                    int j;
                    for(j=0; j<t-1; j++)
                    {
                        strcat(directorio, arreglo2[j]);
                        strcat(directorio, "/");
                        mkdir(directorio, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
                    }
                    
                    //SECREA IMAGEN
                }
            }
            else if(strcmp("exec",arreglo[x]) == 0)
            {
                //leer la ruta del archivo solo mandan ruta para leer el archivo de entrada
            }
            else if(strcmp("x",arreglo[x]) == 0)
            {
                ex = 1;
            }
            else
            {
                printf("Este comando no existe %s \n", arreglo[x]);
            }
            
            int y;
            for(y=0; y<i; y++)
            {
                strcpy(arreglo[y], "");
            }
            i = 0;
        }
    }
    return (EXIT_SUCCESS);
}


// convierte de mayusculas a minuscula
void str_lw( char* s )
{
    while( *s ){
    *s = tolower((int)*s);
    s++;
    }
}

long filesize(FILE *f)
{
  long curpos, length;
  curpos = ftell(f); /* ftell devuelve la posición del cursor del fichero */
  fseek(f, 0L, SEEK_END);
  length = ftell(f);
  fseek(f, curpos, SEEK_SET);
  return length;
}

char* FechaHora()
{
    time_t tAct = time(NULL);
    //struct tm *tiempolocal = localtime(&tiempo);
    char *fechahora;
    fechahora = asctime(localtime(&tAct));
   // strftime(fechahora,128, "%d/%m/%y %H:%M:%S", tiempolocal);
    return fechahora;
}
